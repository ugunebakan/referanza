import ListPage from './pages/ListPage';
import NewUploadPage from './pages/NewUploadPage';
import SinglePage from './pages/SinglePage';
import NotFound from './pages/NotFound';

export default [
    { path: '/', component: ListPage, name: 'list'},
    { path: '/new', component: NewUploadPage, name: 'new'},
    { path: '/gif/:id(\\d+)', component: SinglePage, name: 'single' },
    { path: '/404', component: NotFound },
    { path: '*', redirect: '/404' }
];


