import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import routes from './routes';
import VueMaterial from 'vue-material';
import 'vue-material/dist/vue-material.min.css';
import { MdButton, MdContent, MdToolbar } from 'vue-material/dist/components';

import axios from 'axios';
import VueAxios from 'vue-axios';

import vue2Dropzone from 'vue2-dropzone';
import 'vue2-dropzone/dist/vue2Dropzone.min.css';

Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

Vue.use(vue2Dropzone);
Vue.use(VueRouter);
Vue.use(VueMaterial);
Vue.use(MdButton);
Vue.use(MdContent);
Vue.use(MdToolbar);

const router = new VueRouter({
    routes,
    mode: 'history'
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');

