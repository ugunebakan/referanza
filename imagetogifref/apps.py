from django.apps import AppConfig


class ImagetogifrefConfig(AppConfig):
    name = 'imagetogifref'
