import base64
import io

from PIL import Image


def from_base64_to_bytes(string):
    imagedata = base64.b64decode(string.split('base64,')[1])
    return io.BytesIO(imagedata)


def images_to_gif(images, duration=500):
    _images = []

    for image in images:
        frame = Image.open(from_base64_to_bytes(image))
        _images.append(frame)

    _buffer = io.BytesIO()
    _images[0].save(
        _buffer,
        save_all=True,
        append_images=_images[1:],
        duration=duration,
        loop=0,
        format='GIF'
    )
    gif = 'data:image/gif;base64,{0}'.format(
        base64.b64encode(_buffer.getvalue()).decode('utf-8')
    )

    return gif
