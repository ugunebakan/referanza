from rest_framework import routers
from imagetogifref.views import GifViewSet


router = routers.SimpleRouter()
router.register(r'', GifViewSet, base_name='gifs')

urlpatterns = router.urls
