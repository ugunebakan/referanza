from rest_framework import viewsets

from imagetogifref.models import Gif
from imagetogifref.serializers import (
    GifSerializer,
    GifListSerializer
)


class GifViewSet(viewsets.ModelViewSet):

    queryset = Gif.objects.all()
    serializer_class = GifSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return GifListSerializer

        return self.serializer_class
