from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()


class Gif(models.Model):
    user = models.ForeignKey(
        User,
        null=True,
        on_delete = 'DO_NOTHING'
    )
    gif_image_file = models.TextField()
    created_at = models.DateTimeField(
        auto_now_add=True
    )


class Images(models.Model):
    file_name = models.CharField(
        max_length=255
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    image_file = models.TextField()
    gif = models.ForeignKey(
        Gif,
        on_delete = 'DO_NOTHING',
        related_name='gif_images'
    )
