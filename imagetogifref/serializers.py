from django.db import transaction
from rest_framework import serializers
from imagetogifref.models import (
    Gif,
    Images
)
from imagetogifref.helpers import images_to_gif


class ImageSerializer(serializers.ModelSerializer):
    base64 = serializers.CharField(source='image_file')
    name = serializers.CharField(source='file_name')

    class Meta:
        model = Images
        fields = [
            'base64',
            'name',
        ]


class GifListSerializer(serializers.ModelSerializer):
    created_at = serializers.ReadOnlyField()
    user = serializers.ReadOnlyField()
    gif_image_file = serializers.ReadOnlyField()

    class Meta:
        model = Gif
        fields = [
            'id',
            'created_at',
            'user',
            'gif_image_file',
        ]


class GifSerializer(serializers.ModelSerializer):
    images = ImageSerializer(many=True, source='gif_images')
    duration = serializers.IntegerField(default=500, write_only=True)
    created_at = serializers.ReadOnlyField()
    user = serializers.ReadOnlyField()
    gif_image_file = serializers.ReadOnlyField()

    class Meta:
        model = Gif
        fields = [
            'images',
            'duration',
            'created_at',
            'user',
            'gif_image_file',
        ]

    def validate_images(self, value):
        if not value:
            raise serializers.ValidationError(
                {'images': 'You must include at least one image.'}
            )
        else:
            return value

    def create(self, validated_data):
        images = [obj.get(
            'image_file'
        ) for obj in validated_data.get(
                                        'gif_images'
                                        )]
        gif_string = images_to_gif(images)
        with transaction.atomic():
            gif_obj = Gif.objects.create(
                gif_image_file=gif_string
            )
            for image in validated_data.get('gif_images'):
                Images.objects.create(
                        gif=gif_obj,
                        file_name=image.get('file_name'),
                        image_file=image.get('image_file')
                )

        return gif_obj
