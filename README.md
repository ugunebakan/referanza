# Referanza Test

cloning the dependency:

    git clone git@bitbucket.org:ugunebakan/referanza.git


## Installing Dependencies:
Open two terminal windows
npm (window1):

    cd referanza
    npm run install
Python (window2):

    cd referanza
    virtualenv env -p python
    source env/bin/activate
    pip install -r requirements.txt
    python manage.py migrate

## Running The Application:
1. Run `python manage.py runserver` for backend server
2. Run `npm run serve` for frontend server
3. You can access the application from http://localhost:8080/

## TODO:
Backlog of this application can be found at:
https://trello.com/b/cpMBsu3B/ref


